package com.good.id.demo.service;

import com.goodid.demo.client.PersonalDataClient;
import com.goodid.demo.domain.Customer;
import com.goodid.demo.domain.client.PersonalData;
import com.goodid.demo.service.PersonalDataVerificationService;
import com.goodid.demo.transformer.PersonalDataTransformer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;

import static org.mockito.Mockito.*;

public class PersonalDataVerificationServiceTest {

    public static final String LAST_NAME = "LastName";
    public static final String FIRST_NAME = "FirstName";
    public static final String NAME_OF_MOTHER = "NameOfMother";
    public static final LocalDate BIRTHDAY = LocalDate.of(1995, 1, 1);
    public static final String PLACE_OF_BIRTH = "PlaceOfBirth";
    public static final String DOCUMENT_ID = "DocumentID";
    public static final String DOCUMENT_FRONT_IMAGE = "DocumentFrontImage";
    public static final String SELFIE_IMAGE = "SelfieImage";
    public static final String SUCCESS = "SUCCESS";
    public static final String FAILED = "FAILED";

    @InjectMocks
    private PersonalDataVerificationService underTest;

    @Mock
    private PersonalDataClient personalDataClient;
    @Mock
    private PersonalDataTransformer personalDataTransformer;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void verifyPersonalDataShouldReturnTrueWhenVerifyWasSuccess() {
        //GIVEN
        var customer = getCustomer();
        var personalData = getPersonalData();
        Mockito.when(personalDataTransformer.transform(customer)).thenReturn(personalData);
        Mockito.when(personalDataClient.verify(personalData)).thenReturn(SUCCESS);

        //WHEN
        var actual = underTest.verifyPersonalData(customer);

        //THEN
        verify(personalDataTransformer, times(1)).transform(customer);
        verify(personalDataClient, times(1)).verify(personalData);
        verifyNoMoreInteractions(personalDataTransformer, personalDataClient);
        Assert.assertTrue(actual);
    }

    @Test
    public void verifyPersonalDataShouldReturnFalseWhenVerifyWasFailed() {
        //GIVEN
        var customer = getCustomer();
        var personalData = getPersonalData();
        Mockito.when(personalDataTransformer.transform(customer)).thenReturn(personalData);
        Mockito.when(personalDataClient.verify(personalData)).thenReturn(FAILED);

        //WHEN
        var actual = underTest.verifyPersonalData(customer);

        //THEN
        verify(personalDataTransformer, times(1)).transform(customer);
        verify(personalDataClient, times(1)).verify(personalData);
        verifyNoMoreInteractions(personalDataTransformer, personalDataClient);
        Assert.assertFalse(actual);
    }

    private Customer getCustomer() {
        return Customer.builder()
                .withLastName(LAST_NAME)
                .withFirstName(FIRST_NAME)
                .withNameOfMother(NAME_OF_MOTHER)
                .withBirthday(BIRTHDAY)
                .withPlaceOfBirth(PLACE_OF_BIRTH)
                .withDocumentID(DOCUMENT_ID)
                .withDocumentFrontImage(DOCUMENT_FRONT_IMAGE)
                .withSelfieImage(SELFIE_IMAGE)
                .build();
    }

    private PersonalData getPersonalData() {
        return PersonalData.PersonalDataBuilder.builder()
                .withLastName(LAST_NAME)
                .withFirstName(FIRST_NAME)
                .withPlaceOfBirth(PLACE_OF_BIRTH)
                .withBirthday(BIRTHDAY)
                .withNameOfMother(NAME_OF_MOTHER)
                .build();
    }
}
