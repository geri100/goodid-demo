package com.good.id.demo.service.customer;

import com.goodid.demo.domain.Customer;
import com.goodid.demo.domain.CustomerEntity;
import com.goodid.demo.repository.CustomerRepository;
import com.goodid.demo.service.customer.CustomerService;
import com.goodid.demo.service.customer.CustomerVerificationService;
import com.goodid.demo.transformer.CustomerTransformer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class CustomerServiceTest {
    public static final String LAST_NAME = "LastName";
    public static final String FIRST_NAME = "FirstName";
    public static final String NAME_OF_MOTHER = "NameOfMother";
    public static final LocalDate BIRTHDAY = LocalDate.of(1995, 1, 1);
    public static final String PLACE_OF_BIRTH = "PlaceOfBirth";
    public static final String DOCUMENT_ID = "DocumentID";
    public static final String DOCUMENT_FRONT_IMAGE = "DocumentFrontImage";
    public static final String SELFIE_IMAGE = "SelfieImage";
    public static final String SUCCESS = "SUCCESS";
    public static final String FAILED = "FAILED";
    public static final int VERIFICATION_TIMEOUT = 0;

    @InjectMocks
    private CustomerService underTest;

    @Mock
    private CustomerRepository customerRepository;
    @Mock
    private CustomerTransformer customerTransformer;
    @Mock
    private CustomerVerificationService customerVerificationService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getStatusShouldReturnSuccessWhenRepositoryContainEntity() {
        //GIVEN
        var customer = getCustomer(DOCUMENT_FRONT_IMAGE, SELFIE_IMAGE);
        Mockito.when(customerRepository
                .findByLastNameAndFirstNameAndBirthdayAndPlaceOfBirthAndNameOfMother(LAST_NAME, FIRST_NAME,
                        BIRTHDAY, PLACE_OF_BIRTH, NAME_OF_MOTHER)).thenReturn(getCustomerEntity(true, true));

        //WHEN
        var actual = underTest.getStatus(customer);

        //THEN
        Mockito.verify(customerRepository, times(1)).findByLastNameAndFirstNameAndBirthdayAndPlaceOfBirthAndNameOfMother(LAST_NAME, FIRST_NAME, BIRTHDAY, PLACE_OF_BIRTH, NAME_OF_MOTHER);
        verifyNoMoreInteractions(customerRepository);
        Assert.assertEquals(SUCCESS, actual);
    }

    @Test
    public void getStatusShouldReturnFailedWhenEntityNull() {
        //GIVEN
        var customer = getCustomer(DOCUMENT_FRONT_IMAGE, SELFIE_IMAGE);
        Mockito.when(customerRepository
                .findByLastNameAndFirstNameAndBirthdayAndPlaceOfBirthAndNameOfMother(LAST_NAME, FIRST_NAME,
                        BIRTHDAY, PLACE_OF_BIRTH, NAME_OF_MOTHER)).thenReturn(null);

        //WHEN
        var actual = underTest.getStatus(customer);

        //THEN
        Mockito.verify(customerRepository, times(1)).findByLastNameAndFirstNameAndBirthdayAndPlaceOfBirthAndNameOfMother(LAST_NAME, FIRST_NAME, BIRTHDAY, PLACE_OF_BIRTH, NAME_OF_MOTHER);
        verifyNoMoreInteractions(customerRepository);
        Assert.assertEquals(FAILED, actual);
    }

    @Test
    public void getStatusShouldReturnFailedWhenDocumentNotVerified() {
        //GIVEN
        var customer = getCustomer(DOCUMENT_FRONT_IMAGE, SELFIE_IMAGE);
        Mockito.when(customerRepository
                .findByLastNameAndFirstNameAndBirthdayAndPlaceOfBirthAndNameOfMother(LAST_NAME, FIRST_NAME,
                        BIRTHDAY, PLACE_OF_BIRTH, NAME_OF_MOTHER)).thenReturn(getCustomerEntity(false, true));

        //WHEN
        var actual = underTest.getStatus(customer);

        //THEN
        Mockito.verify(customerRepository, times(1)).findByLastNameAndFirstNameAndBirthdayAndPlaceOfBirthAndNameOfMother(LAST_NAME, FIRST_NAME, BIRTHDAY, PLACE_OF_BIRTH, NAME_OF_MOTHER);
        verifyNoMoreInteractions(customerRepository);
        Assert.assertEquals(FAILED, actual);
    }

    @Test
    public void getStatusShouldReturnFailedWhenPersonalDataNotVerified() {
        //GIVEN
        var customer = getCustomer(DOCUMENT_FRONT_IMAGE, SELFIE_IMAGE);
        Mockito.when(customerRepository
                .findByLastNameAndFirstNameAndBirthdayAndPlaceOfBirthAndNameOfMother(LAST_NAME, FIRST_NAME,
                        BIRTHDAY, PLACE_OF_BIRTH, NAME_OF_MOTHER)).thenReturn(getCustomerEntity(true, false));

        //WHEN
        var actual = underTest.getStatus(customer);

        //THEN
        Mockito.verify(customerRepository, times(1)).findByLastNameAndFirstNameAndBirthdayAndPlaceOfBirthAndNameOfMother(LAST_NAME, FIRST_NAME, BIRTHDAY, PLACE_OF_BIRTH, NAME_OF_MOTHER);
        verifyNoMoreInteractions(customerRepository);
        Assert.assertEquals(FAILED, actual);
    }

    @Test
    public void getStatusShouldReturnFailedWhenPersonalDataAndDocumentNotVerified() {
        //GIVEN
        var customer = getCustomer(DOCUMENT_FRONT_IMAGE, SELFIE_IMAGE);
        Mockito.when(customerRepository
                .findByLastNameAndFirstNameAndBirthdayAndPlaceOfBirthAndNameOfMother(LAST_NAME, FIRST_NAME,
                        BIRTHDAY, PLACE_OF_BIRTH, NAME_OF_MOTHER)).thenReturn(getCustomerEntity(false, false));

        //WHEN
        var actual = underTest.getStatus(customer);

        //THEN
        Mockito.verify(customerRepository, times(1)).findByLastNameAndFirstNameAndBirthdayAndPlaceOfBirthAndNameOfMother(LAST_NAME, FIRST_NAME, BIRTHDAY, PLACE_OF_BIRTH, NAME_OF_MOTHER);
        verifyNoMoreInteractions(customerRepository);
        Assert.assertEquals(FAILED, actual);
    }

    @Test
    public void getCustomerTimeoutShouldReturnTimeWhenCustomerExist() {
        //GIVEN
        var customer = getCustomer(DOCUMENT_FRONT_IMAGE, SELFIE_IMAGE);
        Mockito.when(customerRepository
                .findByLastNameAndFirstNameAndBirthdayAndPlaceOfBirthAndNameOfMother(LAST_NAME, FIRST_NAME,
                        BIRTHDAY, PLACE_OF_BIRTH, NAME_OF_MOTHER)).thenReturn(getCustomerEntity(false, false));

        //WHEN
        var actual = underTest.getCustomerTimeout(customer);

        //THEN
        Mockito.verify(customerRepository, times(1)).findByLastNameAndFirstNameAndBirthdayAndPlaceOfBirthAndNameOfMother(LAST_NAME, FIRST_NAME, BIRTHDAY, PLACE_OF_BIRTH, NAME_OF_MOTHER);
        verifyNoMoreInteractions(customerRepository);
        Assert.assertEquals(LocalDateTime.MAX, actual);
    }

    @Test
    public void verifyShouldReturnSuccessWhenImagesReadyAndSavedIntoDatabase() {
        //GIVEN
        var customer = getCustomer(DOCUMENT_FRONT_IMAGE, SELFIE_IMAGE);
        var customerEntity = getCustomerEntity(true, true);
        Mockito.when(customerVerificationService.verifyDocumentation(customer)).thenReturn(true);
        Mockito.when(customerVerificationService.verifyPersonalData(customer)).thenReturn(true);
        Mockito.when(customerTransformer.transformToEntity(customer, VERIFICATION_TIMEOUT)).thenReturn(customerEntity);
        Mockito.when(customerRepository.save(customerEntity)).thenReturn(customerEntity);

        //WHEN
        var actual = underTest.verify(customer);

        //THEN
        Mockito.verify(customerVerificationService, times(1)).verifyDocumentation(customer);
        Mockito.verify(customerVerificationService, times(1)).verifyPersonalData(customer);
        Mockito.verify(customerTransformer, times(1)).transformToEntity(customer, VERIFICATION_TIMEOUT);
        Mockito.verify(customerRepository, times(1)).save(customerEntity);
        verifyNoMoreInteractions(customerVerificationService, customerTransformer, customerRepository);
        Assert.assertEquals(SUCCESS, actual);
    }

    @Test
    public void verifyShouldReturnSuccessWhenImagesNOTReadyAndSavedIntoDatabase() {
        //GIVEN
        var customer = getCustomer(null, null);
        var customerEntity = getCustomerEntity(false, false);
        Mockito.when(customerTransformer.transformToEntity(customer, VERIFICATION_TIMEOUT)).thenReturn(customerEntity);
        Mockito.when(customerRepository.save(customerEntity)).thenReturn(customerEntity);

        //WHEN
        var actual = underTest.verify(customer);

        //THEN
        Mockito.verify(customerRepository, times(1)).save(customerEntity);
        Mockito.verify(customerTransformer, times(1)).transformToEntity(customer, VERIFICATION_TIMEOUT);
        verifyNoMoreInteractions(customerVerificationService, customerTransformer, customerRepository);
        Assert.assertEquals(SUCCESS, actual);
    }

    @Test
    public void verifyShouldReturnSuccessWhenSelfieNOTReadyAndSavedIntoDatabase() {
        //GIVEN
        var customer = getCustomer(null, SELFIE_IMAGE);
        var customerEntity = getCustomerEntity(false, false);
        Mockito.when(customerTransformer.transformToEntity(customer, VERIFICATION_TIMEOUT)).thenReturn(customerEntity);
        Mockito.when(customerRepository.save(customerEntity)).thenReturn(customerEntity);

        //WHEN
        var actual = underTest.verify(customer);

        //THEN
        Mockito.verify(customerTransformer, times(1)).transformToEntity(customer, VERIFICATION_TIMEOUT);
        Mockito.verify(customerRepository, times(1)).save(customerEntity);
        verifyNoMoreInteractions(customerVerificationService, customerTransformer, customerRepository);
        Assert.assertEquals(SUCCESS, actual);
    }

    @Test
    public void verifyShouldReturnSuccessWhenDocumentNOTReadyAndSavedIntoDatabase() {
        //GIVEN
        var customer = getCustomer(DOCUMENT_FRONT_IMAGE, null);
        var customerEntity = getCustomerEntity(false, false);
        Mockito.when(customerTransformer.transformToEntity(customer, VERIFICATION_TIMEOUT)).thenReturn(customerEntity);
        Mockito.when(customerRepository.save(customerEntity)).thenReturn(customerEntity);

        //WHEN
        var actual = underTest.verify(customer);

        //THEN
        Mockito.verify(customerTransformer, times(1)).transformToEntity(customer, VERIFICATION_TIMEOUT);

        Mockito.verify(customerRepository, times(1)).save(customerEntity);
        verifyNoMoreInteractions(customerVerificationService, customerTransformer, customerRepository);
        Assert.assertEquals(SUCCESS, actual);
    }

    @Test
    public void verifyShouldReturnFailedWhenImagesReadyAndCANNOTSavedIntoDatabase() {
        //GIVEN
        var customer = getCustomer(DOCUMENT_FRONT_IMAGE, SELFIE_IMAGE);
        var customerEntity = getCustomerEntity(true, true);
        Mockito.when(customerVerificationService.verifyDocumentation(customer)).thenReturn(true);
        Mockito.when(customerVerificationService.verifyPersonalData(customer)).thenReturn(true);
        Mockito.when(customerTransformer.transformToEntity(customer, VERIFICATION_TIMEOUT)).thenReturn(customerEntity);
        Mockito.when(customerRepository.save(customerEntity)).thenThrow(RuntimeException.class);

        //WHEN
        var actual = underTest.verify(customer);

        //THEN
        Mockito.verify(customerVerificationService, times(1)).verifyDocumentation(customer);
        Mockito.verify(customerVerificationService, times(1)).verifyPersonalData(customer);
        Mockito.verify(customerTransformer, times(1)).transformToEntity(customer, VERIFICATION_TIMEOUT);
        Mockito.verify(customerRepository, times(1)).save(customerEntity);
        verifyNoMoreInteractions(customerVerificationService, customerTransformer, customerRepository);
        Assert.assertEquals(FAILED, actual);

    }

    @Test
    public void refreshCustomerShouldReturnSuccess() {
        //GIVEN
        var customer = getCustomer(DOCUMENT_FRONT_IMAGE, SELFIE_IMAGE);
        var customerEntity = getCustomerEntity(true, true);
        Mockito.when(customerVerificationService.verifyDocumentation(customer)).thenReturn(true);
        Mockito.when(customerVerificationService.verifyPersonalData(customer)).thenReturn(true);
        Mockito.when(customerRepository.save(customerEntity)).thenReturn(customerEntity);
        Mockito.when(customerRepository
                .findByLastNameAndFirstNameAndBirthdayAndPlaceOfBirthAndNameOfMother(LAST_NAME, FIRST_NAME,
                        BIRTHDAY, PLACE_OF_BIRTH, NAME_OF_MOTHER)).thenReturn(customerEntity);


        //WHEN
        var actual = underTest.refreshCustomer(customer);

        //THEN
        Mockito.verify(customerRepository, times(1)).findByLastNameAndFirstNameAndBirthdayAndPlaceOfBirthAndNameOfMother(LAST_NAME, FIRST_NAME, BIRTHDAY, PLACE_OF_BIRTH, NAME_OF_MOTHER);
        Mockito.verify(customerVerificationService, times(1)).verifyDocumentation(customer);
        Mockito.verify(customerVerificationService, times(1)).verifyPersonalData(customer);
        Mockito.verify(customerRepository, times(1)).save(customerEntity);
        verifyNoMoreInteractions(customerVerificationService, customerTransformer, customerRepository);
        Assert.assertEquals(SUCCESS, actual);
    }

    @Test
    public void refreshCustomerShouldReturnFailed() {
        //GIVEN
        var customer = getCustomer(DOCUMENT_FRONT_IMAGE, SELFIE_IMAGE);
        var customerEntity = getCustomerEntity(true, true);
        Mockito.when(customerVerificationService.verifyDocumentation(customer)).thenReturn(true);
        Mockito.when(customerVerificationService.verifyPersonalData(customer)).thenReturn(true);
        Mockito.when(customerRepository.save(customerEntity)).thenThrow(RuntimeException.class);
        Mockito.when(customerRepository
                .findByLastNameAndFirstNameAndBirthdayAndPlaceOfBirthAndNameOfMother(LAST_NAME, FIRST_NAME,
                        BIRTHDAY, PLACE_OF_BIRTH, NAME_OF_MOTHER)).thenReturn(customerEntity);


        //WHEN
        var actual = underTest.refreshCustomer(customer);

        //THEN
        Mockito.verify(customerRepository, times(1)).findByLastNameAndFirstNameAndBirthdayAndPlaceOfBirthAndNameOfMother(LAST_NAME, FIRST_NAME, BIRTHDAY, PLACE_OF_BIRTH, NAME_OF_MOTHER);
        Mockito.verify(customerVerificationService, times(1)).verifyDocumentation(customer);
        Mockito.verify(customerVerificationService, times(1)).verifyPersonalData(customer);
        Mockito.verify(customerRepository, times(1)).save(customerEntity);
        verifyNoMoreInteractions(customerVerificationService, customerTransformer, customerRepository);
        Assert.assertEquals(FAILED, actual);
    }

    private Customer getCustomer(String documentImage, String selfie) {
        return Customer.builder()
                .withLastName(LAST_NAME)
                .withFirstName(FIRST_NAME)
                .withNameOfMother(NAME_OF_MOTHER)
                .withBirthday(BIRTHDAY)
                .withPlaceOfBirth(PLACE_OF_BIRTH)
                .withDocumentID(DOCUMENT_ID)
                .withDocumentFrontImage(documentImage)
                .withSelfieImage(selfie)
                .build();
    }

    private CustomerEntity getCustomerEntity(boolean documentVerified, boolean personalDataVerified) {
        var entity = new CustomerEntity();
        entity.setId(0L);
        entity.setLastName(LAST_NAME);
        entity.setFirstName(FIRST_NAME);
        entity.setNameOfMother(NAME_OF_MOTHER);
        entity.setBirthday(BIRTHDAY);
        entity.setPlaceOfBirth(PLACE_OF_BIRTH);
        entity.setDocumentID(DOCUMENT_ID);
        entity.setDocumentFrontImage(DOCUMENT_FRONT_IMAGE);
        entity.setSelfieImage(SELFIE_IMAGE);
        entity.setDocumentVerified(documentVerified);
        entity.setPersonalDataVerified(personalDataVerified);
        entity.setTimeout(LocalDateTime.MAX);

        return entity;
    }
}
