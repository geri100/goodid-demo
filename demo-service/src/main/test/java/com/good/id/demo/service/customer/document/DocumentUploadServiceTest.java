package com.good.id.demo.service.customer.document;

import com.goodid.demo.domain.Customer;
import com.goodid.demo.service.customer.CustomerService;
import com.goodid.demo.service.customer.document.DocumentUploadService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.mockito.Mockito.*;

public class DocumentUploadServiceTest {

    public static final String LAST_NAME = "LastName";
    public static final String FIRST_NAME = "FirstName";
    public static final String NAME_OF_MOTHER = "NameOfMother";
    public static final LocalDate BIRTHDAY = LocalDate.of(1995, 1, 1);
    public static final String PLACE_OF_BIRTH = "PlaceOfBirth";
    public static final String DOCUMENT_ID = "DocumentID";
    public static final String DOCUMENT_FRONT_IMAGE = "DocumentFrontImage";
    public static final String SELFIE_IMAGE = "SelfieImage";
    public static final String SUCCESS = "SUCCESS";
    public static final String FAILED = "FAILED";

    @InjectMocks
    private DocumentUploadService underTest;

    @Mock
    private CustomerService customerService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void uploadDocumentShouldReturnSuccessWhenNotExpiredAndContainsDocuments() {
        //GIVEN
        var customer = getCustomer();
        Mockito.when(customerService.getCustomerTimeout(customer)).thenReturn(LocalDateTime.MAX);
        Mockito.when(customerService.refreshCustomer(customer)).thenReturn(SUCCESS);

        //WHEN
        var actual = underTest.uploadDocument(customer);

        //THEN
        verify(customerService, times(1)).getCustomerTimeout(customer);
        verify(customerService, times(1)).refreshCustomer(customer);
        verifyNoMoreInteractions(customerService);
        Assert.assertEquals(SUCCESS, actual);
    }

    @Test
    public void uploadDocumentShouldReturnFailedWhenExpired() {
        //GIVEN
        var customer = getCustomer();
        Mockito.when(customerService.getCustomerTimeout(customer)).thenReturn(LocalDateTime.MIN);

        //WHEN
        var actual = underTest.uploadDocument(customer);

        //THEN
        verify(customerService, times(1)).getCustomerTimeout(customer);
        verifyNoMoreInteractions(customerService);
        Assert.assertEquals(FAILED, actual);
    }

    @Test
    public void uploadDocumentShouldReturnFailedWhenVerifyFailed() {
        //GIVEN
        var customer = getCustomer();
        Mockito.when(customerService.getCustomerTimeout(customer)).thenReturn(LocalDateTime.MAX);
        Mockito.when(customerService.refreshCustomer(customer)).thenReturn(FAILED);

        //WHEN
        var actual = underTest.uploadDocument(customer);

        //THEN
        verify(customerService, times(1)).getCustomerTimeout(customer);
        verify(customerService, times(1)).refreshCustomer(customer);
        verifyNoMoreInteractions(customerService);
        Assert.assertEquals(FAILED, actual);
    }

    private Customer getCustomer() {
        return Customer.builder()
                .withLastName(LAST_NAME)
                .withFirstName(FIRST_NAME)
                .withNameOfMother(NAME_OF_MOTHER)
                .withBirthday(BIRTHDAY)
                .withPlaceOfBirth(PLACE_OF_BIRTH)
                .withDocumentID(DOCUMENT_ID)
                .withDocumentFrontImage(DOCUMENT_FRONT_IMAGE)
                .withSelfieImage(SELFIE_IMAGE)
                .build();
    }

}
