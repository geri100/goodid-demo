package com.good.id.demo.service.customer.document;

import com.goodid.demo.client.DocumentClient;
import com.goodid.demo.domain.Customer;
import com.goodid.demo.domain.client.Document;
import com.goodid.demo.service.customer.document.DocumentVerificationService;
import com.goodid.demo.transformer.DocumentTransformer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;

import static org.mockito.Mockito.*;

public class DocumentVerificationServiceTest {

    public static final String LAST_NAME = "LastName";
    public static final String FIRST_NAME = "FirstName";
    public static final String NAME_OF_MOTHER = "NameOfMother";
    public static final LocalDate BIRTHDAY = LocalDate.of(1995, 1, 1);
    public static final String PLACE_OF_BIRTH = "PlaceOfBirth";
    public static final String DOCUMENT_ID = "DocumentID";
    public static final String DOCUMENT_FRONT_IMAGE = "DocumentFrontImage";
    public static final String SELFIE_IMAGE = "SelfieImage";
    public static final String SUCCESS_RATE_DOCUMENT = "95%";
    public static final String FAILED_RATE_DOCUMENT = "50%";

    @InjectMocks
    private DocumentVerificationService underTest;

    @Mock
    private DocumentClient documentClient;
    @Mock
    private DocumentTransformer documentTransformer;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void verifyDocumentationShouldReturnTrueWhenVerifyWasSuccess() {
        //GIVEN
        var customer = getCustomer();
        var document = getDocument();
        Mockito.when(documentTransformer.transform(customer)).thenReturn(document);
        Mockito.when(documentClient.verify(document)).thenReturn(SUCCESS_RATE_DOCUMENT);

        //WHEN
        var actual = underTest.verifyDocumentation(customer);

        //THEN
        verify(documentTransformer, times(1)).transform(customer);
        verify(documentClient, times(1)).verify(document);
        verifyNoMoreInteractions(documentTransformer, documentClient);
        Assert.assertTrue(actual);
    }

    @Test
    public void verifyDocumentationShouldReturnFalseWhenVerifyWasFailed() {
        //GIVEN
        var customer = getCustomer();
        var document = getDocument();
        Mockito.when(documentTransformer.transform(customer)).thenReturn(document);
        Mockito.when(documentClient.verify(document)).thenReturn(FAILED_RATE_DOCUMENT);

        //WHEN
        var actual = underTest.verifyDocumentation(customer);

        //THEN
        verify(documentTransformer, times(1)).transform(customer);
        verify(documentClient, times(1)).verify(document);
        verifyNoMoreInteractions(documentTransformer, documentClient);
        Assert.assertFalse(actual);
    }

    private Customer getCustomer() {
        return Customer.builder()
                .withLastName(LAST_NAME)
                .withFirstName(FIRST_NAME)
                .withNameOfMother(NAME_OF_MOTHER)
                .withBirthday(BIRTHDAY)
                .withPlaceOfBirth(PLACE_OF_BIRTH)
                .withDocumentID(DOCUMENT_ID)
                .withDocumentFrontImage(DOCUMENT_FRONT_IMAGE)
                .withSelfieImage(SELFIE_IMAGE)
                .build();
    }

    private Document getDocument() {
        return new Document(SELFIE_IMAGE, DOCUMENT_FRONT_IMAGE);
    }
}
