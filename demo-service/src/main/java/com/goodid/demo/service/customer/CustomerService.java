package com.goodid.demo.service.customer;

import com.goodid.demo.domain.CustomerEntity;
import com.goodid.demo.repository.CustomerRepository;
import com.goodid.demo.domain.Customer;
import com.goodid.demo.transformer.CustomerTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class CustomerService {

    public static final String SUCCESS = "SUCCESS";
    public static final String FAILED = "FAILED";

    @Value( "${verification.timeout}" )
    private long verificationTimeout;

    private final CustomerRepository customerRepository;
    private final CustomerTransformer customerTransformer;
    private final CustomerVerificationService customerVerificationService;

    @Autowired
    public CustomerService(CustomerRepository customerRepository, CustomerTransformer customerTransformer, CustomerVerificationService customerVerificationService) {
        this.customerRepository = customerRepository;
        this.customerTransformer = customerTransformer;
        this.customerVerificationService = customerVerificationService;
    }

    public String getStatus(Customer customer) {
        var entity = findCustomer(customer);

        return entity != null &&
                entity.isDocumentVerified() &&
                entity.isPersonalDataVerified() ? SUCCESS : FAILED;
    }

    public String verify(Customer customer) {
        boolean documentSuccess = false;
        boolean personalDataSuccess = false;
        if(readyToVerify(customer)) {
            documentSuccess = customerVerificationService.verifyDocumentation(customer);
            personalDataSuccess = customerVerificationService.verifyPersonalData(customer);
        }

        return save(customer, documentSuccess, personalDataSuccess) ? SUCCESS : FAILED;
    }

    public String refreshCustomer(Customer customer) {
        try {
            if (readyToVerify(customer)) {
                var entity = findCustomer(customer);
                entity.setDocumentVerified(customerVerificationService.verifyDocumentation(customer));
                entity.setPersonalDataVerified(customerVerificationService.verifyPersonalData(customer));
                customerRepository.save(entity);
            }
        } catch (Exception e) {
            return FAILED;
        }

        return SUCCESS;
    }

    public LocalDateTime getCustomerTimeout(Customer customer) {
        return findCustomer(customer).getTimeout();
    }

    private CustomerEntity findCustomer(Customer customer) {
        return customerRepository.findByLastNameAndFirstNameAndBirthdayAndPlaceOfBirthAndNameOfMother(
                customer.getLastName(),
                customer.getFirstName(),
                customer.getBirthday(),
                customer.getPlaceOfBirth(),
                customer.getNameOfMother());
    }

    private boolean readyToVerify(Customer customer) {
        return customer.getDocumentFrontImage() != null &&
                customer.getSelfieImage() != null;
    }

    private boolean save(Customer customer, boolean documentSuccess, boolean personalDataSuccess) {
        var customerEntity = customerTransformer.transformToEntity(customer, verificationTimeout);
        customerEntity.setDocumentVerified(documentSuccess);
        customerEntity.setPersonalDataVerified(personalDataSuccess);
        try {
            customerRepository.save(customerEntity);
        } catch (Exception exception) {
            //LOG
            return false;
        }
        return true;
    }

}
