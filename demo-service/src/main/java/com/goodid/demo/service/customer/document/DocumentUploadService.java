package com.goodid.demo.service.customer.document;

import com.goodid.demo.domain.Customer;
import com.goodid.demo.service.customer.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class DocumentUploadService {

    public static final String FAILED = "FAILED";

    private final CustomerService customerService;

    @Autowired
    public DocumentUploadService(CustomerService customerService) {
        this.customerService = customerService;
    }

    public String uploadDocument(Customer customer) {
        var timeout = customerService.getCustomerTimeout(customer);

        return notExpired(timeout)
                ? customerService.refreshCustomer(customer)
                : FAILED;
    }

    private boolean notExpired(LocalDateTime expirationDate) {
        return LocalDateTime.now().isBefore(expirationDate);
    }
}
