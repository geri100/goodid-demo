package com.goodid.demo.service;

import com.goodid.demo.client.PersonalDataClient;
import com.goodid.demo.domain.Customer;
import com.goodid.demo.transformer.PersonalDataTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonalDataVerificationService {

    private static final String SUCCESS = "SUCCESS";

    private final PersonalDataClient personalDataClient;
    private final PersonalDataTransformer personalDataTransformer;

    @Autowired
    public PersonalDataVerificationService(PersonalDataClient personalDataClient, PersonalDataTransformer personalDataTransformer) {
        this.personalDataClient = personalDataClient;
        this.personalDataTransformer = personalDataTransformer;
    }

    public boolean verifyPersonalData(Customer customer) {
        var personalData = personalDataTransformer.transform(customer);
        return SUCCESS.equals(personalDataClient.verify(personalData));
    }

}
