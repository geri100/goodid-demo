package com.goodid.demo.service.customer.document;

import com.goodid.demo.client.DocumentClient;
import com.goodid.demo.domain.Customer;
import com.goodid.demo.transformer.DocumentTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DocumentVerificationService {

    private static final String SUCCESS_RATE_DOCUMENT = "95%";

    private final DocumentClient documentClient;
    private final DocumentTransformer documentTransformer;

    @Autowired
    public DocumentVerificationService(DocumentClient documentClient, DocumentTransformer documentTransformer) {
        this.documentClient = documentClient;
        this.documentTransformer = documentTransformer;
    }

    public boolean verifyDocumentation(Customer customer) {
        var document = documentTransformer.transform(customer);
        return SUCCESS_RATE_DOCUMENT.equals(documentClient.verify(document));
    }

}
