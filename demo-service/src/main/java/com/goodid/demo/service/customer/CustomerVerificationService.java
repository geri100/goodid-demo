package com.goodid.demo.service.customer;

import com.goodid.demo.domain.Customer;
import com.goodid.demo.service.customer.document.DocumentVerificationService;
import com.goodid.demo.service.PersonalDataVerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerVerificationService {

    private final DocumentVerificationService documentVerificationService;
    private final PersonalDataVerificationService personalDataVerificationService;

    @Autowired
    public CustomerVerificationService(DocumentVerificationService documentVerificationService, PersonalDataVerificationService personalDataVerificationService) {
        this.documentVerificationService = documentVerificationService;
        this.personalDataVerificationService = personalDataVerificationService;
    }

    public boolean verifyDocumentation(Customer customer) {
        return documentVerificationService.verifyDocumentation(customer);
    }

    public boolean verifyPersonalData(Customer customer) {
        return personalDataVerificationService.verifyPersonalData(customer);
    }
}
