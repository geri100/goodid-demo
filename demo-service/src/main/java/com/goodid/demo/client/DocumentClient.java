package com.goodid.demo.client;

import com.goodid.demo.domain.client.Document;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "document-verify", url = "${document.verify.url}")
public interface DocumentClient {

    @RequestMapping(method = RequestMethod.GET)
    String verify(Document document);
}
