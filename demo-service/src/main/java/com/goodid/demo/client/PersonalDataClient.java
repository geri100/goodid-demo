package com.goodid.demo.client;

import com.goodid.demo.domain.client.PersonalData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "personal-data-verify", url = "${personal.data.verify.url}")
public interface PersonalDataClient {

    @RequestMapping(method = RequestMethod.POST)
    String verify(PersonalData personalData);
}
