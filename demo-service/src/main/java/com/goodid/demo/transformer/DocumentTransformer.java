package com.goodid.demo.transformer;

import com.goodid.demo.domain.Customer;
import com.goodid.demo.domain.client.Document;
import org.springframework.stereotype.Service;

@Service
public class DocumentTransformer {

    public Document transform(Customer customer) {
        return new Document(customer.getSelfieImage(), customer.getDocumentFrontImage());
    }
}
