package com.goodid.demo.transformer;

import com.goodid.demo.domain.Customer;
import com.goodid.demo.domain.client.PersonalData;
import org.springframework.stereotype.Service;

@Service
public class PersonalDataTransformer {

    public PersonalData transform(Customer customer) {
        return PersonalData.PersonalDataBuilder.builder()
                .withLastName(customer.getLastName())
                .withFirstName(customer.getFirstName())
                .withNameOfMother(customer.getNameOfMother())
                .withBirthday(customer.getBirthday())
                .withPlaceOfBirth(customer.getPlaceOfBirth())
                .build();
    }
}
