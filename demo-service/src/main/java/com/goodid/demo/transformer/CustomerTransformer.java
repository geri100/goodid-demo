package com.goodid.demo.transformer;

import com.goodid.demo.domain.CustomerEntity;
import com.goodid.demo.domain.Customer;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class CustomerTransformer {

    public CustomerEntity transformToEntity(Customer customer, long verificationTimeout) {
        var entity = new CustomerEntity();
        entity.setLastName(customer.getLastName());
        entity.setFirstName(customer.getFirstName());
        entity.setNameOfMother(customer.getNameOfMother());
        entity.setBirthday(customer.getBirthday());
        entity.setPlaceOfBirth(customer.getPlaceOfBirth());
        entity.setDocumentID(customer.getDocumentID());
        entity.setDocumentFrontImage(customer.getDocumentFrontImage());
        entity.setSelfieImage(customer.getSelfieImage());
        entity.setTimeout(LocalDateTime.now().plusSeconds(verificationTimeout));
        return entity;
    }
}
