package com.goodid.demo.domain.client;

import java.time.LocalDate;
import java.util.StringJoiner;

public class PersonalData {

    private String lastName;
    private String firstName;
    private String nameOfMother;
    private LocalDate birthday;
    private String placeOfBirth;

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getNameOfMother() {
        return nameOfMother;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", PersonalData.class.getSimpleName() + "[", "]")
                .add("lastName='" + lastName + "'")
                .add("firstName='" + firstName + "'")
                .add("nameOfMother='" + nameOfMother + "'")
                .add("birthday=" + birthday)
                .add("placeOfBirth='" + placeOfBirth + "'")
                .toString();
    }

    public static final class PersonalDataBuilder {
        private String lastName;
        private String firstName;
        private String nameOfMother;
        private LocalDate birthday;
        private String placeOfBirth;

        private PersonalDataBuilder() {
        }

        public static PersonalDataBuilder builder() {
            return new PersonalDataBuilder();
        }

        public PersonalDataBuilder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public PersonalDataBuilder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public PersonalDataBuilder withNameOfMother(String nameOfMother) {
            this.nameOfMother = nameOfMother;
            return this;
        }

        public PersonalDataBuilder withBirthday(LocalDate birthday) {
            this.birthday = birthday;
            return this;
        }

        public PersonalDataBuilder withPlaceOfBirth(String placeOfBirth) {
            this.placeOfBirth = placeOfBirth;
            return this;
        }

        public PersonalData build() {
            PersonalData personalData = new PersonalData();
            personalData.firstName = this.firstName;
            personalData.lastName = this.lastName;
            personalData.nameOfMother = this.nameOfMother;
            personalData.birthday = this.birthday;
            personalData.placeOfBirth = this.placeOfBirth;
            return personalData;
        }
    }
}
