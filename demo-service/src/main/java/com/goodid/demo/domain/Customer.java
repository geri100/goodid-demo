package com.goodid.demo.domain;

import java.time.LocalDate;
import java.util.Objects;
import java.util.StringJoiner;

public class Customer {

    private String lastName;
    private String firstName;
    private String nameOfMother;
    private LocalDate birthday;
    private String placeOfBirth;
    private String documentID;
    private String documentFrontImage;
    private String selfieImage;

    public Customer(String lastName, String firstName, String nameOfMother, LocalDate birthday, String placeOfBirth, String documentID, String documentFrontImage, String selfieImage) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.nameOfMother = nameOfMother;
        this.birthday = birthday;
        this.placeOfBirth = placeOfBirth;
        this.documentID = documentID;
        this.documentFrontImage = documentFrontImage;
        this.selfieImage = selfieImage;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getNameOfMother() {
        return nameOfMother;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public String getDocumentID() {
        return documentID;
    }

    public String getDocumentFrontImage() {
        return documentFrontImage;
    }

    public String getSelfieImage() {
        return selfieImage;
    }

    public static CustomerBuilder builder() {
        return new CustomerBuilder();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Customer)) return false;
        Customer customer = (Customer) o;
        return Objects.equals(lastName, customer.lastName) && Objects.equals(firstName, customer.firstName) && Objects.equals(nameOfMother, customer.nameOfMother) && Objects.equals(birthday, customer.birthday) && Objects.equals(placeOfBirth, customer.placeOfBirth) && Objects.equals(documentID, customer.documentID) && Objects.equals(documentFrontImage, customer.documentFrontImage) && Objects.equals(selfieImage, customer.selfieImage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastName, firstName, nameOfMother, birthday, placeOfBirth, documentID, documentFrontImage, selfieImage);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Customer.class.getSimpleName() + "[", "]")
                .add("lastName='" + lastName + "'")
                .add("firstName='" + firstName + "'")
                .add("nameOfMother='" + nameOfMother + "'")
                .add("birthday=" + birthday)
                .add("placeOfBirth='" + placeOfBirth + "'")
                .add("documentID='" + documentID + "'")
                .add("documentFrontImage='" + documentFrontImage + "'")
                .add("selfieImage='" + selfieImage + "'")
                .toString();
    }

    public static final class CustomerBuilder {
        private String lastName;
        private String firstName;
        private String nameOfMother;
        private LocalDate birthday;
        private String placeOfBirth;
        private String documentID;
        private String documentFrontImage;
        private String selfieImage;

        private CustomerBuilder() {
        }

        public static CustomerBuilder aCustomer() {
            return new CustomerBuilder();
        }

        public CustomerBuilder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public CustomerBuilder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public CustomerBuilder withNameOfMother(String nameOfMother) {
            this.nameOfMother = nameOfMother;
            return this;
        }

        public CustomerBuilder withBirthday(LocalDate birthday) {
            this.birthday = birthday;
            return this;
        }

        public CustomerBuilder withPlaceOfBirth(String placeOfBirth) {
            this.placeOfBirth = placeOfBirth;
            return this;
        }

        public CustomerBuilder withDocumentID(String documentID) {
            this.documentID = documentID;
            return this;
        }

        public CustomerBuilder withDocumentFrontImage(String documentFrontImage) {
            this.documentFrontImage = documentFrontImage;
            return this;
        }

        public CustomerBuilder withSelfieImage(String selfieImage) {
            this.selfieImage = selfieImage;
            return this;
        }

        public Customer build() {
            return new Customer(lastName, firstName, nameOfMother, birthday, placeOfBirth, documentID, documentFrontImage, selfieImage);
        }
    }
}
