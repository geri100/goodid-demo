package com.goodid.demo.domain.client;

public class Document {

    private String selfie;
    private String document;

    public Document(String selfie, String document) {
        this.selfie = selfie;
        this.document = document;
    }

    public String getSelfie() {
        return selfie;
    }

    public void setSelfie(String selfie) {
        this.selfie = selfie;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }
}
