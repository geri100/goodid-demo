# goodid-demo


## Run
From IntelliJ just run the  `CustomerVerificationApplication.java`. 

###Database
The database run under the `localhost:3306/demo`.


###Wiremock
We use wiremock which uses the 8090 port so it need to be free.
The wiremock failed if coudn't match the request. See under` DocumentMock.java`

###Set timeout
`Application.properties` file contains `verification.timeout` key value pair. Just change the value.(value represent in seconds)


##Endpoints
I use postman and invoke the URLs:

- verification: `localhost:8080/verification`
    - POST
    - JSON
    - Response
        - **success**: saved into DB
        - **failed**: can not saved into DB or sth went wrong
- status: `localhost:8080/status`
  - GET
  - JSON
  - Response
      - **success**: verification completed
      - **failed**: verification uncompleted OR denied
- upload document: `localhost:8080/upload/document`
  - POST
  - JSON
  - Response
      - **success**: document upload complated
      - **failed**: something went wrong during uploading
    
We got a dummy response that it was success or failed.

JSON example:
`{
"lastName":  "last",
"firstName":  "first",
"nameOfMother":  "mother",
"birthday":  "1995-01-09",
"placeOfBirth":  "place",
"documentID":  "doksi_id",
"documentFrontImage":  "doksi_hash",
"selfieImage":  "selfi_hash"
}`