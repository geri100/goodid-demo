package com.goodid.demo.domain;

import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.StringJoiner;

@Entity
@Table(name = "customer", schema = "demo")
public class CustomerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "name_of_mother")
    private String nameOfMother;
    @Column(name = "birthday")
    private LocalDate birthday;
    @Column(name = "place_of_birth")
    private String placeOfBirth;
    @Column(name = "document_id")
    private String documentID;
    @Column(name = "document_image")
    private String documentFrontImage;
    @Column(name = "selfie")
    private String selfieImage;
    @Column(name = "timeout")
    private LocalDateTime timeout;
    @Column(name = "document_verified")
    private boolean documentVerified;
    @Column(name = "personal_data_verified")
    private boolean personalDataVerified;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getNameOfMother() {
        return nameOfMother;
    }

    public void setNameOfMother(String nameOfMother) {
        this.nameOfMother = nameOfMother;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getDocumentID() {
        return documentID;
    }

    public void setDocumentID(String documentID) {
        this.documentID = documentID;
    }

    public String getDocumentFrontImage() {
        return documentFrontImage;
    }

    public void setDocumentFrontImage(String documentFrontImage) {
        this.documentFrontImage = documentFrontImage;
    }

    public String getSelfieImage() {
        return selfieImage;
    }

    public void setSelfieImage(String selfieImage) {
        this.selfieImage = selfieImage;
    }

    public LocalDateTime getTimeout() {
        return timeout;
    }

    public void setTimeout(LocalDateTime timeout) {
        this.timeout = timeout;
    }

    public boolean isDocumentVerified() {
        return documentVerified;
    }

    public void setDocumentVerified(boolean documentVerified) {
        this.documentVerified = documentVerified;
    }

    public boolean isPersonalDataVerified() {
        return personalDataVerified;
    }

    public void setPersonalDataVerified(boolean personalDataVerified) {
        this.personalDataVerified = personalDataVerified;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CustomerEntity.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("lastName='" + lastName + "'")
                .add("firstName='" + firstName + "'")
                .add("nameOfMother='" + nameOfMother + "'")
                .add("birthday=" + birthday)
                .add("placeOfBirth='" + placeOfBirth + "'")
                .add("documentID='" + documentID + "'")
                .add("documentFrontImage='" + documentFrontImage + "'")
                .add("selfieImage='" + selfieImage + "'")
                .add("timeout=" + timeout)
                .add("documentVerified=" + documentVerified)
                .add("personalDataVerified=" + personalDataVerified)
                .toString();
    }
}
