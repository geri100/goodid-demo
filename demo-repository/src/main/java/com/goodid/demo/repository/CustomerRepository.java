package com.goodid.demo.repository;

import com.goodid.demo.domain.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface CustomerRepository extends JpaRepository<CustomerEntity, Long> {

    CustomerEntity findByLastNameAndFirstNameAndBirthdayAndPlaceOfBirthAndNameOfMother(String lastName, String firstName,
                                                                                       LocalDate birthday, String placeOfBirth,
                                                                                       String nameOfMother);
}
