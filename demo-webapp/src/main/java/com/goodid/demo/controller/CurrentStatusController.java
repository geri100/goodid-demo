package com.goodid.demo.controller;

import com.goodid.demo.domain.CustomerModel;
import com.goodid.demo.facade.CustomerStatusFacade;
import com.goodid.demo.facade.CustomerVerificationFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/")
public class CurrentStatusController {

    private final CustomerStatusFacade customerStatusFacade;

    @Autowired
    public CurrentStatusController(CustomerStatusFacade customerStatusFacade) {
        this.customerStatusFacade = customerStatusFacade;
    }

    @GetMapping(path = "/status", consumes = "application/json", produces = "application/json")
    public String verification(@Valid @RequestBody CustomerModel customerModel) {
        return customerStatusFacade.getStatus(customerModel);
    }
}
