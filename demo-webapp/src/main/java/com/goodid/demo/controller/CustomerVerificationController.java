package com.goodid.demo.controller;

import com.goodid.demo.domain.CustomerModel;
import com.goodid.demo.facade.CustomerVerificationFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/")
public class CustomerVerificationController {

    private final CustomerVerificationFacade customerVerificationFacade;

    @Autowired
    public CustomerVerificationController(CustomerVerificationFacade customerVerificationFacade) {
        this.customerVerificationFacade = customerVerificationFacade;
    }

    @PostMapping(path = "/verification", consumes = "application/json", produces = "application/json")
    public String verification(@Valid @RequestBody CustomerModel customerModel) {
        return customerVerificationFacade.verify(customerModel);
    }
}
