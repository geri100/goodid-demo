package com.goodid.demo.controller;

import com.goodid.demo.domain.CustomerDocumentUploadModel;
import com.goodid.demo.domain.CustomerModel;
import com.goodid.demo.facade.CustomerVerificationFacade;
import com.goodid.demo.facade.DocumentUploadFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/")
public class DocumentUploadController {

    private final DocumentUploadFacade documentUploadFacade;

    @Autowired
    public DocumentUploadController(DocumentUploadFacade documentUploadFacade) {
        this.documentUploadFacade = documentUploadFacade;
    }

    @PostMapping(path = "/upload/document", consumes = "application/json", produces = "application/json")
    public String verification(@Valid @RequestBody CustomerDocumentUploadModel customerDocumentUploadModel) {
        return documentUploadFacade.uploadDocument(customerDocumentUploadModel);
    }
}
