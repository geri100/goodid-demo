package com.goodid.demo.domain;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Objects;
import java.util.StringJoiner;

public class CustomerDocumentUploadModel {

    @NotNull
    @NotBlank
    private String lastName;
    @NotNull
    @NotBlank
    private String firstName;
    @NotNull
    @NotBlank
    private String nameOfMother;
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;
    @NotNull
    @NotBlank
    private String placeOfBirth;
    private String documentFrontImage;
    private String selfieImage;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getNameOfMother() {
        return nameOfMother;
    }

    public void setNameOfMother(String nameOfMother) {
        this.nameOfMother = nameOfMother;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getDocumentFrontImage() {
        return documentFrontImage;
    }

    public void setDocumentFrontImage(String documentFrontImage) {
        this.documentFrontImage = documentFrontImage;
    }

    public String getSelfieImage() {
        return selfieImage;
    }

    public void setSelfieImage(String selfieImage) {
        this.selfieImage = selfieImage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CustomerDocumentUploadModel)) return false;
        CustomerDocumentUploadModel customerModel = (CustomerDocumentUploadModel) o;
        return Objects.equals(lastName, customerModel.lastName) && Objects.equals(firstName, customerModel.firstName) && Objects.equals(nameOfMother, customerModel.nameOfMother) && Objects.equals(birthday, customerModel.birthday) && Objects.equals(placeOfBirth, customerModel.placeOfBirth) && Objects.equals(documentFrontImage, customerModel.documentFrontImage) && Objects.equals(selfieImage, customerModel.selfieImage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastName, firstName, nameOfMother, birthday, placeOfBirth, documentFrontImage, selfieImage);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CustomerDocumentUploadModel.class.getSimpleName() + "[", "]")
                .add("lastName='" + lastName + "'")
                .add("firstName='" + firstName + "'")
                .add("nameOfMother='" + nameOfMother + "'")
                .add("birthday=" + birthday)
                .add("placeOfBirth='" + placeOfBirth + "'")
                .add("documentFrontImage='" + documentFrontImage + "'")
                .add("selfieImage='" + selfieImage + "'")
                .toString();
    }
}
