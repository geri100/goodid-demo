package com.goodid.demo.facade;

import com.goodid.demo.domain.CustomerModel;
import com.goodid.demo.service.customer.CustomerService;
import com.goodid.demo.transformer.CustomerModelTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerStatusFacade {

    private final CustomerService customerService;
    private final CustomerModelTransformer customerModelTransformer;

    @Autowired
    public CustomerStatusFacade(CustomerService customerService, CustomerModelTransformer customerModelTransformer) {
        this.customerService = customerService;
        this.customerModelTransformer = customerModelTransformer;
    }

    public String getStatus(CustomerModel customerModel) {
        return customerService.getStatus(customerModelTransformer.transformToDTO(customerModel));
    }
}
