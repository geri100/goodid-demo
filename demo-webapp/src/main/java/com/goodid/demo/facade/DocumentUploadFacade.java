package com.goodid.demo.facade;

import com.goodid.demo.domain.CustomerDocumentUploadModel;
import com.goodid.demo.service.customer.document.DocumentUploadService;
import com.goodid.demo.transformer.CustomerModelTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DocumentUploadFacade {

    private final DocumentUploadService documentUploadService;
    private final CustomerModelTransformer customerModelTransformer;

    @Autowired
    public DocumentUploadFacade(DocumentUploadService documentUploadService, CustomerModelTransformer customerModelTransformer) {
        this.documentUploadService = documentUploadService;
        this.customerModelTransformer = customerModelTransformer;
    }

    public String uploadDocument(CustomerDocumentUploadModel customerDocumentUploadModel) {
        return documentUploadService.uploadDocument(customerModelTransformer.transformToDTO(customerDocumentUploadModel));
    }
}
