package com.goodid.demo.facade;

import com.goodid.demo.domain.CustomerModel;
import com.goodid.demo.service.customer.CustomerService;
import com.goodid.demo.transformer.CustomerModelTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerVerificationFacade {

    private final CustomerService customerService;
    private final CustomerModelTransformer customerModelTransformer;

    @Autowired
    public CustomerVerificationFacade(CustomerService customerService, CustomerModelTransformer customerModelTransformer) {
        this.customerService = customerService;
        this.customerModelTransformer = customerModelTransformer;
    }

    public String verify(CustomerModel customerModel) {
        return customerService.verify(customerModelTransformer.transformToDTO(customerModel));
    }
}
