package com.goodid.demo.wiremock;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class DocumentMock {

    @Autowired
    private WireMockServer mockService;

    @EventListener(ApplicationReadyEvent.class)
    public void setupMockDocumentResponse() throws IOException {
        documentSuccess();
        documentFailed();
        personalDataSuccess();
        personalDataFailed();
    }

    private StubMapping documentSuccess() {
        return mockService.stubFor(WireMock
                .post(WireMock.urlEqualTo("/document/verify"))
                .withRequestBody(WireMock.equalToJson("{\"selfie\":\"selfi_hash\",\"document\":\"doksi_hash\"}"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                        .withTransformers("response-template")
                        .withBody("95%")));
    }

    private StubMapping documentFailed() {
        return mockService.stubFor(WireMock
                .post(WireMock.urlEqualTo("/document/verify"))
                .withRequestBody(WireMock.equalToJson("{\"selfie\":\"selfie_failed\",\"document\":\"doksi_failed\"}"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                        .withTransformers("response-template")
                        .withBody("50%")));
    }

    private void personalDataSuccess() {
        mockService.stubFor(WireMock
                .post(WireMock.urlEqualTo("/personal-data/verify"))
                .withRequestBody(WireMock.equalToJson("{\"lastName\":\"last\",\"firstName\":\"first\",\"nameOfMother\":\"mother\"," +
                        "\"birthday\":\"1995-01-09\",\"placeOfBirth\" : \"place\"}"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                        .withTransformers("response-template")
                        .withBody("SUCCESS")));
    }

    private void personalDataFailed() {
        mockService.stubFor(WireMock
                .post(WireMock.urlEqualTo("/personal-data/verify"))
                .withRequestBody(WireMock.equalToJson("{\"lastName\":\"failed\",\"firstName\":\"first\",\"nameOfMother\":\"mother\"," +
                        "\"birthday\":\"1995-01-09\",\"placeOfBirth\":\"place\"}"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                        .withTransformers("response-template")
                        .withBody("FAILED")));
    }
}
