package com.goodid.demo.wiremock.config;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.extension.responsetemplating.ResponseTemplateTransformer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

@Configuration
public class WireMockConfig {

    @Value("${wiremock.port}")
    private int wiremockPort;

    @Bean(initMethod = "start", destroyMethod = "stop")
    public WireMockServer mockService() {
        return new WireMockServer(options()
                .extensions(new ResponseTemplateTransformer(false))
                .port(wiremockPort));
    }

}
