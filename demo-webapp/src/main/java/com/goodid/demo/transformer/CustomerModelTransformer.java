package com.goodid.demo.transformer;

import com.goodid.demo.domain.Customer;
import com.goodid.demo.domain.CustomerDocumentUploadModel;
import com.goodid.demo.domain.CustomerModel;
import org.springframework.stereotype.Component;

@Component
public class CustomerModelTransformer {

    public Customer transformToDTO(CustomerModel model) {
        return Customer.builder()
                .withLastName(model.getLastName())
                .withFirstName(model.getFirstName())
                .withNameOfMother(model.getNameOfMother())
                .withBirthday(model.getBirthday())
                .withPlaceOfBirth(model.getPlaceOfBirth())
                .withDocumentID(model.getDocumentID())
                .withDocumentFrontImage(model.getDocumentFrontImage())
                .withSelfieImage(model.getSelfieImage())
                .build();
    }

    public Customer transformToDTO(CustomerDocumentUploadModel model) {
        return Customer.builder()
                .withLastName(model.getLastName())
                .withFirstName(model.getFirstName())
                .withNameOfMother(model.getNameOfMother())
                .withBirthday(model.getBirthday())
                .withPlaceOfBirth(model.getPlaceOfBirth())
                .withDocumentFrontImage(model.getDocumentFrontImage())
                .withSelfieImage(model.getSelfieImage())
                .build();
    }

}
